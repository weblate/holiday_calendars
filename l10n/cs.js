OC.L10N.register(
    "holiday_calendars",
    {
    "Public holidays in {country}" : "Státní svátky v {country}",
    "Calendar holidays" : "Kalendář státních svátků",
    "Easily subscribe to external calendars like public holidays." : "Snadno se přihlaste k odběru externích kalendářů, jako jsou státní svátky.",
    "Add subscription" : "Přidat přihlášení se k odběru",
    "No public holidays calendar subscriptions" : "Žádná přihlášení se k odběru státních svátků",
    "Public holidays" : "Státní svátky",
    "Pick a country" : "Vyberte stát",
    "Religious calendars" : "Náboženské kalendáře",
    "Pick a subscription" : "Vyberte přihlášení se k odběru",
    "School vacations" : "Školní prázdniny",
    "Sources" : "Zdroje",
    "Public holiday calendar list provided by {thunderbirdLink}" : "Seznam kalendářů státních svátků poskytován {thunderbirdLink}",
    "Religious calendar list provided by:" : "Seznam náboženských kalendářů poskytován:",
    "We can't allow adding their calendars directly, but you may find some interesting things to subscribe to on the following websites. Once the feed URL copied, you can subscribe to it directly into the Calendar App." : "Není možné umožnit přímé přidávání jejich kalendářů, ale můžete nalézt zajímavé věci k přihlášení se k odběru na jejich webech. Po zkopírování URL adresy kanálu je možné se přímo přihlásit k odběru v aplikaci Kalednář.",
    "Holiday Calendars" : "Kalednáře státních svátků",
    "Allows to subscribe to holidays public calendars" : "Umožňuje se přihlásit k odběru kalendářů se státními svátky",
    "This is an app listing links to public calendars representing the holidays and allowing to subscribe to them" : "Toto je aplikace vypisující odkazy na veřejné kalendáře, představující státní svátky a umožňující přihlášení se k odběru z nich"
},
"nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;");
