OC.L10N.register(
    "holiday_calendars",
    {
    "Public holidays in {country}" : "Jours fériés en {country}",
    "Calendar holidays" : "Calendrier des jours fériés",
    "Easily subscribe to external calendars like public holidays." : "Abonnez-vous facilement à des calendriers externes tels que les jours fériés.",
    "Add subscription" : "Ajouter un abonnement",
    "No public holidays calendar subscriptions" : "Pas d'abonnements à des calendriers",
    "Public holidays" : "Jours fériés",
    "Pick a country" : "Choisissez un pays",
    "Religious calendars" : "Calendriers religieux",
    "Pick a subscription" : "Choisissez un abonnement",
    "School vacations" : "Vacances scolaires",
    "Sources" : "Sources",
    "Public holiday calendar list provided by {thunderbirdLink}" : "Liste des calendriers des jours fériés fournie par {thunderbirdLink}",
    "Religious calendar list provided by:" : "Liste des calendriers religieux fournie par :",
    "We can't allow adding their calendars directly, but you may find some interesting things to subscribe to on the following websites. Once the feed URL copied, you can subscribe to it directly into the Calendar App." : "Nous ne pouvons pas permettre l'ajout direct de leurs calendriers, mais vous pouvez trouver des choses intéressantes auxquelles vous abonner sur les sites web suivants. Une fois l'URL du flux copiée, vous pouvez vous y abonner directement dans l'application Agenda.",
    "Holiday Calendars" : "Calendrier des jours fériés",
    "Allows to subscribe to holidays public calendars" : "Permet de s'abonner aux calendriers publics des jours fériés",
    "This is an app listing links to public calendars representing the holidays and allowing to subscribe to them" : "Une application répertoriant les liens vers les calendriers publics représentant les fêtes et permettant de s'y abonner"
},
"nplurals=2; plural=(n > 1);");
