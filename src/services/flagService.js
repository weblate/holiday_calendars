import { flag } from 'country-emoji'

/**
 * @param {string} country The country name
 */
export function countryNameToFlag(country) {
	switch (country) {
	case 'Finland (Swedish)':
		return flag('Finland')
	case 'Flanders':
		return flag('Belgium')
	case 'Kazakhstan (Russian)':
		return flag('Kazakhstan')
	case 'UK [Northern Ireland]':
		return flag('UK')
	default:
		return flag(country)
	}
}
