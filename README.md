# Nextcloud Holiday Calendars

An app listing links to public calendars representing the holidays and allowing to subscribe to them.

![](screenshots/list_subscriptions.png)
![](screenshots/add_new_subscription.png)
