const webpackConfig = require('@nextcloud/webpack-vue-config')
const path = require('path')

module.exports = {
	...webpackConfig,
	entry: {
		personalSettings: path.join(__dirname, 'src/PersonalSettings.js'),
	},
}
