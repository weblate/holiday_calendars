<?php

declare(strict_types=1);

namespace OCA\HolidayCalendars\Tests\Settings;

use ChristophWurst\Nextcloud\Testing\TestCase;
use OCA\HolidayCalendars\AppInfo\Application;
use OCA\HolidayCalendars\Settings\Personal;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Services\IInitialState;
use PHPUnit\Framework\MockObject\MockObject;

class PersonalTest extends TestCase {
	private Personal $personal;
	/**
	 * @var IInitialState|MockObject
	 */
	private $initialState;

	public function setUp(): void {
		parent::setUp();
		$this->initialState = $this->createMock(IInitialState::class);

		$this->personal = new Personal($this->initialState);
	}

	public function testGetPriority(): void {
		$this->assertEquals(40, $this->personal->getPriority());
	}

	public function testGetSection(): void {
		$this->assertEquals('groupware', $this->personal->getSection());
	}

	public function testGetForm(): void {
		$res = new TemplateResponse(Application::APP_ID, 'personal');

		$this->initialState->expects($this->exactly(3))->method('provideInitialState')->withConsecutive(
			['holiday_calendars', $this->anything()],
			['religious_calendars', $this->anything()],
			['school_holidays', $this->anything()]
		);

		$this->assertEquals($res, $this->personal->getForm());
	}
}
